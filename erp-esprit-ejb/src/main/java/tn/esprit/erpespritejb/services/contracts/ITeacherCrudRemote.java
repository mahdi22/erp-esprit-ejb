package tn.esprit.erpespritejb.services.contracts;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.erpespritejb.entites.Teacher;

@Remote
public interface ITeacherCrudRemote {

	public void addTeacher(Teacher teacher);
	
	public void updateTeacher(Teacher teacher);
	
	public void deleteTeacher(int id);
	
	public void deleteTeacher(Teacher teacher);
	
	public Teacher findTeacherById(int id);
	
	public List<Teacher> findAllTeacher();
}
