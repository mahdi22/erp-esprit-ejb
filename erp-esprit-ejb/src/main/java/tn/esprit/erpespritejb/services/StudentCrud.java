package tn.esprit.erpespritejb.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.erpespritejb.entites.Student;
import tn.esprit.erpespritejb.services.contracts.IStudentCrudRemote;

@Stateless
@LocalBean
public class StudentCrud implements IStudentCrudRemote {

	@PersistenceContext
	EntityManager em;

	public void addStudent(Student student) {
		em.persist(student);
		
	}

	@Override
	public void updateStudent(Student student) {
		em.merge(student);
	}

	@Override
	public void deleteStudent(int id) {
		em.remove(findStudentById(id));
	}

	@Override
	public void deleteStudent(Student student) {
		em.remove(student);
		
	}

	@Override
	public Student findStudentById(int id) {
		return em.find(Student.class, id);
	}

	@Override
	public List<Student> findAllStudent() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	

}
