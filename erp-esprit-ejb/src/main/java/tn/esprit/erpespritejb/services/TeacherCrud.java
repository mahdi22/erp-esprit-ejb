package tn.esprit.erpespritejb.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.erpespritejb.entites.Teacher;
import tn.esprit.erpespritejb.services.contracts.ITeacherCrudRemote;

@Stateless
@LocalBean
public class TeacherCrud implements ITeacherCrudRemote {

	@PersistenceContext
	EntityManager em;

	public void addTeacher(Teacher teacher) {
		em.persist(teacher);
		
	}

	@Override
	public void updateTeacher(Teacher teacher) {
		em.merge(teacher);
	}

	@Override
	public void deleteTeacher(int id) {
		em.remove(findTeacherById(id));
	}

	@Override
	public void deleteTeacher(Teacher teacher) {
		em.remove(teacher);
		
	}

	@Override
	public Teacher findTeacherById(int id) {
		return em.find(Teacher.class, id);
	}

	@Override
	public List<Teacher> findAllTeacher() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	

}
