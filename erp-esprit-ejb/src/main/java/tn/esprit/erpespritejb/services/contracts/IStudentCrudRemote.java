package tn.esprit.erpespritejb.services.contracts;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.erpespritejb.entites.Student;

@Remote
public interface IStudentCrudRemote {

	public void addStudent(Student student);
	
	public void updateStudent(Student student);
	
	public void deleteStudent(int id);
	
	public void deleteStudent(Student student);
	
	public Student findStudentById(int id);
	
	public List<Student> findAllStudent();
}
