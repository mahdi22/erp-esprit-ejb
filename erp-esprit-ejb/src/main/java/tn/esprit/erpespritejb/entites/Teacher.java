package tn.esprit.erpespritejb.entites;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "t_teacher")
public class Teacher extends Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float salary;
	private List<Module> modules;

	public Teacher() {
		// TODO Auto-generated constructor stub
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	@OneToMany(mappedBy = "teacher")
	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

}
