package tn.esprit.erpespritejb.entites;

import javax.persistence.Embeddable;

@Embeddable
public class ClassRoom {

	private String labelClassRoom;
	private boolean elevator;
	private Classe classe;

	public ClassRoom(String labelClassRoom, boolean elevator, Classe classe) {
		super();
		this.labelClassRoom = labelClassRoom;
		this.elevator = elevator;
		this.classe = classe;
	}

	public ClassRoom() {
		// TODO Auto-generated constructor stub
	}

	public String getLabelClassRoom() {
		return labelClassRoom;
	}

	public void setLabelClassRoom(String labelClassRoom) {
		this.labelClassRoom = labelClassRoom;
	}

	public boolean isElevator() {
		return elevator;
	}

	public void setElevator(boolean elevator) {
		this.elevator = elevator;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

}
