package tn.esprit.erpespritejb.entites;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_module")
public class Module {

	private ModulePK pk;
	private int hours;
	private String label;
	private int ectsNumber;
	private Teacher teacher;
	private Classe classe;

	public Module() {
		// TODO Auto-generated constructor stub
	}

	@EmbeddedId
	public ModulePK getPk() {
		return pk;
	}

	public void setPk(ModulePK pk) {
		this.pk = pk;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getEctsNumber() {
		return ectsNumber;
	}

	public void setEctsNumber(int ectsNumber) {
		this.ectsNumber = ectsNumber;
	}

	@ManyToOne
	@JoinColumn(referencedColumnName = "id", name = "idTeacher", insertable = false, updatable = false)
	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	@ManyToOne
	@JoinColumn(referencedColumnName = "id", name = "idClasse", insertable = false, updatable = false)
	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

}
