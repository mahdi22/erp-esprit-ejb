package tn.esprit.erpespritejb.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_student")
public class Student extends Person implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean credit;
	private Classe classe;

	public Student(String firstName, String lastName, Date birthdate,
			boolean gender, boolean credit) {
		super(firstName, lastName, birthdate, gender);
		this.credit = credit;
	}

	public Student(int id, String firstName, String lastName, Date birthdate,
			boolean gender, boolean credit) {
		super(id, firstName, lastName, birthdate, gender);
		this.credit = credit;
	}

	public Student() {
	}

	public boolean isCredit() {
		return credit;
	}

	public void setCredit(boolean credit) {
		this.credit = credit;
	}

	@ManyToOne
	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

}
