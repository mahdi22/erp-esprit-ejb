package tn.esprit.erpespritejb.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "t_classe")
public class Classe implements Serializable {

	private int id;
	private String label;
	private String scholarYear;
	private Curuculum curuculum;

	private List<Student> students;
	private List<Module> modules;

	private ClassRoom classRoom;
	private static final long serialVersionUID = 1L;

	public Classe() {

	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getScholarYear() {
		return this.scholarYear;
	}

	public void setScholarYear(String scholarYear) {
		this.scholarYear = scholarYear;
	}

	public Curuculum getCuruculum() {
		return curuculum;
	}

	public void setCuruculum(Curuculum curuculum) {
		this.curuculum = curuculum;
	}

	@Embedded
	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}

	@OneToMany(mappedBy = "classe")
	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	@OneToMany(mappedBy = "classe")
	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

}
